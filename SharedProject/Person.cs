﻿using System;
namespace SharedProject
{
    public class Person
    {
        public Person()
        {
            id = Guid.NewGuid().ToString();
        }

        public String firstName { get; set; }
        public String lastName { get; set; }
        public int age { get; set; }
        public String id { get; } // let the constructor set this

    }
}
