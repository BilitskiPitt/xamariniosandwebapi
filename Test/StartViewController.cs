﻿using System;
using System.Net.Http;
using UIKit;
using Newtonsoft;
using Newtonsoft.Json;
using SharedProject;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public partial class StartViewController : UIViewController
    {
        public StartViewController() : base("StartViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }



        async partial void PostButton_TouchUpInside(UIButton sender)
        {
            HttpClient client;
            client = new HttpClient();
            var uri = new Uri(string.Format("http://localhost:5000/api/Person"));
            Person p = new Person();
            p.firstName = "Jim";
            p.lastName = "Bilitski";
            p.age = 25;

            var json = JsonConvert.SerializeObject(p);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(uri, content);
            if (response.IsSuccessStatusCode)
            {
                var ac = UIAlertController.Create("POST", "Complete", UIAlertControllerStyle.Alert);
                ac.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                InvokeOnMainThread(
                    () =>
                        {
                            this.PresentViewController(ac, true, null);
                        }
                );


            }
        }

        async partial void GetButton_TouchUpInside(UIButton sender)
        {
            HttpClient client;
            client = new HttpClient();


            var uri = new Uri(string.Format("http://localhost:5000/api/Person"));

            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var Items = JsonConvert.DeserializeObject<List<Person>>(content);
                if (Items != null)
                {
                    var ac = UIAlertController.Create("GET", Items.Count + " records", UIAlertControllerStyle.Alert);
                    ac.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                    InvokeOnMainThread(() =>
                    {
                        this.PresentViewController(ac, true, null);
                    }
                    );

                }
            }
        }
    }
}

