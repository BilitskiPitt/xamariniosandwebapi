﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Test
{
    [Register ("StartViewController")]
    partial class StartViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton getButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton postButton { get; set; }

        [Action ("GetButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void GetButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("PostButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void PostButton_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (getButton != null) {
                getButton.Dispose ();
                getButton = null;
            }

            if (postButton != null) {
                postButton.Dispose ();
                postButton = null;
            }
        }
    }
}