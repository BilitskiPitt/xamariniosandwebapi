﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using SharedProject;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPIProject.Controllers
{
    [Route("api/[controller]")]
    public class PersonController : Controller
    {

        // improve this by making a singleton data storage layer
        //   instead of storing in ram
        private static List<Person> people = new List<Person>();


        // GET: api/values
        [HttpGet]
        public List<Person> Get()
        {
            return people;
        }

        // GET api/values/SOME_GUID_STRING
        [HttpGet("{id}")]
        public Person Get(String id)
        {
            // get a person by it's id
            return people.Where(p => p.id == id).FirstOrDefault();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Person value)
        {
            // TODO:  Add any logic:  eg to check for duplicates, valid fields, etc
            people.Add(value);

            // TODO: return non void if you want and send back data for success/fail
        }

        // PUT api/values/SOME_STRING_GUID
        //    body has has the person object
        [HttpPut("{id}")]
        public void Put(String id, [FromBody]Person value)
        {
            var personToEdit = people.Where(p => p.id == id).FirstOrDefault();
            if (personToEdit != null)
            {
                // I could just delete old then add new
                //  but I made it so the constructor makes the IDs

                // ok this person exists, now edit it
                personToEdit.age = value.age;
                personToEdit.firstName = value.firstName;
                personToEdit.lastName = value.lastName;
            }
            else
            {
                // return a non void and send back item does not exists error code
            }
        }

        // DELETE api/values/SOME_STRING_GUID
        [HttpDelete("{id}")]
        public void Delete(String id)
        {
            var personToDelete = people.Where(p => p.id == id).FirstOrDefault();
            if (personToDelete != null)
            {
                people.Remove(personToDelete);
            }


        }
    }
}
